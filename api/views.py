from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework import generics, status
from rest_framework.decorators import permission_classes
from rest_framework.permissions import AllowAny
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.pagination import LimitOffsetPagination 
from rest_framework.views import APIView
from rest_framework.parsers import FileUploadParser 
from rest_framework.response import Response

import json
import csv 
import random

from api.models import *
from api.serializers import *

# CUSTOM PAGINATION
class CustomPagination(LimitOffsetPagination):
    default_limit = 15

@permission_classes((AllowAny, ))
class UserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    pagination_class = CustomPagination

class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class BankList(generics.ListCreateAPIView):
    queryset = Bank.objects.all()
    serializer_class = BankSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filter_fields = '__all__'
    ordering = ('id',)
    pagination_class = CustomPagination

class BankDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Bank.objects.all()
    serializer_class = BankSerializer

class BatchList(generics.ListCreateAPIView):
    queryset = Batch.objects.all()
    serializer_class = BatchSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filter_fields = '__all__'
    ordering = ('id',)
    pagination_class = CustomPagination

class BatchDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Batch.objects.all()
    serializer_class = BatchSerializer

class BatchRecord(generics.ListCreateAPIView):
    queryset = BatchRecord.objects.all()
    serializer_class = BatchRecordSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filter_fields = '__all__'
    ordering = ('id',)
    pagination_class = CustomPagination

# Custom View to Handle File Upload
@permission_classes((AllowAny, ))
class BatchUpload(APIView):
    parser_classes = (FileUploadParser ,)

    def post(self, request, format=None):

        if 'file' not in request.data:
            resp = {"success": False, "data": "File missing or Failed to upload file"}
        else:
            bstatus = Status.objects.get(id=1)
            currency = Currency.objects.latest('id')
            bank = Bank.objects.latest('id')
            f = request.data['file']
            file = File(
                name = f.name,
                file = f
            )
            file.save()
            batch = Batch(
                batch_id = random.randint(100000, 999999),
                batch_number = random.randint(100000, 999999),
                batch_status = bstatus,
                currency = currency,
                file = file,
                count = 1,
                bank = bank,
            )
            batch.save()

            resp = {"success": True, "data": "File upload successful"}

        return Response(resp, status=status.HTTP_200_OK)
