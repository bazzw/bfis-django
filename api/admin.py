from django.contrib import admin

from api.models import *

# Register your models here.
admin.site.register(Bank)
admin.site.register(Currency)
admin.site.register(Status)
admin.site.register(Batch)
admin.site.register(File)
