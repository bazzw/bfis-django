from django.db import models

# Create your models here.
class Bank(models.Model):
    name = models.CharField(max_length=255, default='', blank=False)
    code = models.CharField(max_length=255, default='', blank=False)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

class Status(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=255, default='')
    code = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.code

    class Meta:
        verbose_name_plural = 'Status\''

class Currency(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255, default='')
    short_name = models.CharField(max_length=255, default='')
    code = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Currencies'

class File(models.Model):
    name = models.CharField(max_length=255, default='')
    file = models.FileField(upload_to='', default='')

    def __str__(self):
        return self.name

class Batch(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)
    batch_id = models.CharField(max_length=255, default='')
    batch_number = models.CharField(max_length=255, default='')
    batch_status = models.ForeignKey(Status, on_delete=models.CASCADE)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    file = models.ForeignKey(File, on_delete=models.CASCADE, blank=True, null=True)
    count = models.IntegerField(default=1)
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)

    def __str__(self):
        return '%s %s' % (self.bank, self.batch_id)

    class Meta:
        verbose_name_plural = 'Batches'

class BatchRecord(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)
    batch = models.ForeignKey(Batch, on_delete=models.CASCADE)
    account_number_credit = models.CharField(max_length=255, default='')
    account_number_debit = models.CharField(max_length=255, default='')
    amount = models.IntegerField(default=0)
    beneficiary_bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    beneficiary_name = models.CharField(max_length=255, default='')
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    reference = models.CharField(max_length=255, default='')
    remitter_name = models.CharField(max_length=255, default='')
    
    def __str__(self):
        return self.beneficiary_name