from rest_framework import serializers
from django.contrib.auth.models import User
import json
import random

from api.models import *

class UserSerializer(serializers.ModelSerializer):
    """ Version serializer """
    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        print('validated data', validated_data)
        user = User(
            email=validated_data['email'],
            username=validated_data['username'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        user.set_password(validated_data['password'])
        user.save()

        return user

class BankSerializer(serializers.ModelSerializer):
    """ Version serializer """
    class Meta:
        model = Bank
        fields = '__all__'

class StatusSerializer(serializers.ModelSerializer):
    """ Version serializer """
    class Meta:
        model = Status
        fields = '__all__'

class FileSerializer(serializers.ModelSerializer):
    """ Version serializer """
    class Meta:
        model = File
        fields = '__all__'

class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = '__all__'

class BatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Batch
        fields = '__all__'

class BatchRecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = BatchRecord
        fields = '__all__'
