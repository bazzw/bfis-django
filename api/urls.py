from django.contrib import admin
from django.urls import path
from django.conf.urls import include

from api.views import *

urlpatterns = [
    path('users/', UserList.as_view()),
    path('users/<int:pk>/', UserDetail.as_view()),
    path('banks/', BankList.as_view()),
    path('banks/<int:pk>/', BankDetail.as_view()),
    path('batches/', BatchList.as_view()),
    path('batches/<int:pk>/', BatchDetail.as_view()),
    path('batchrecords/', BatchRecord.as_view()),
    path('batchupload/', BatchUpload.as_view()),
] 
